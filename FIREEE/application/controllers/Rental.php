<?php
class Rental extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->load->model('rental_model');
    }
    
    public function index()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('username', 'Username', 'required');
        $data['title'] = "Menu";
        
        $data['menu'] = array(
            array(
                'title' => 'Admin',
                'location' => 'admin_menu'
            ),
            array(
                'title' => 'User',
                'location' => 'user_menu'
            )
        );
        if ($this->session->has_userdata('error')) {
            $data['error'] = $this->session->userdata('error');
            $this->session->unset_userdata('error');
        }
        else {
            $data['error'] = '';
        }
        $this->load->view('templates/header', $data);
        $this->load->view('rental/login', $data);
        $this->load->view('rental/menu', $data);
        $this->load->view('templates/footer');
    }
    
    public function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $temp = $this->rental_model->find_user($username);
        if ($temp  === null) {
            $this->session->set_userdata('error', "Wrong username or password");
            redirect('rental');
        }
        else {
            if ($temp['password'] == $password) {
                $this->session->set_userdata('username', $username);
                if ($temp['permission']) {
                    $this->session->set_userdata('permission', 1);
                    redirect('rental/admin_menu');
                }
                else {
                    $this->session->set_userdata('permission', 0);
                    redirect('rental/user_menu');
                }
            }
            else {
                $this->session->set_userdata('error', "Wrong username or password");
                redirect('rental');
            }
        }
        
    }
    
    public function show_cars()
    {
        $data['title'] = "List";
        
        
        $this->load->view("templates/header", $data);
        $this->to_menu('admin_menu');
        
        $this->table($data);
        
        $this->to_menu('admin_menu');
        $this->load->view("templates/footer");
    }
    
    public function show_res($slug = null)
    {
        $this->login_test(1);
        $data['title'] = "Reservations";
        $this->load->view('templates/header', $data);
        $this->to_menu('admin_menu');
        
        if ($slug === null) {
            
            $data['options'] = array(
                array(
                    'title' => 'show res',
                    'location' => 'show_res'
                )
            );

            $this->table($data);
        }
        else {
            $data['res'] = $this->rental_model->show_res($slug);
            $this->load->view('rental/show_res', $data);
        }
        $this->to_menu('admin_menu');
        $this->load->view("templates/footer");
    }
    
    public function del_car($slug = null)
    {
        $this->login_test(1);
        if ($slug === null)
        {
            $data['title'] = "Del car";
            $data['options'] = array(
                array(
                    'title' => 'del',
                    'location' => 'del_car'
                )
            );
            $this->load->view("templates/header", $data);
            $this->to_menu('admin_menu');

            $this->table($data);

            $this->to_menu('admin_menu');
            $this->load->view("templates/footer");
        }
        else
        {
            $temp = $this->rental_model->del_car($slug);
        
            if ($temp)
            {
                $data['title'] = "Car deleted";
            }
            else
            {
                $data['title'] = "Error";
            }

            $this->load->view("templates/header", $data);
            $this->to_menu('admin_menu');
            $this->load->view("templates/footer");
        }
    }
    
    public function add_car()
    {
        $this->login_test(1);
        $this->form_options();
        
        $data['title'] = "Create car";
        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('rental/add_car', array('error' => ''));
        }
        else
        {
            if (!$this->upload->do_upload('image')) {
                
                $error = array('error' => $this->upload->display_errors());
                
                $this->load->view('templates/header', $data);
                $this->load->view('rental/add_car', $error);
            }
            else
            {
                if (1!= 1) {
                    $error = array('error' => 'This is not image');
                
                    $this->load->view('templates/header', $data);
                    $this->load->view('rental/add_car', $error);
                }
                else
                {
                    $data['title'] = "Car added";
                    $file_name = $this->upload->data('file_name');
                    $this->rental_model->add_car($file_name);
                    $this->load->view('templates/header', $data);
                }
                
            }
        }
        
        $this->to_menu('admin_menu');
        $this->load->view('templates/footer');
    }
    
    public function edit_car($slug = null)
    {
        $this->login_test(1);
        if ($slug === null)
        {
            $data['title'] = "Edit Car";
            $data['options'] = array(
                array(
                    'title' => 'edit',
                    'location' => 'edit_car'
                )
            );
            
            $this->load->view('templates/header', $data);
            $this->to_menu('admin_menu');
            
            $this->table($data);
        }
        else
        {
            $this->load->helper('url');
            $this->form_options();
            
            $data['title'] = "Edit car";
            $data['car'] = $this->rental_model->get_cars($slug);
            

            if ($this->form_validation->run() === FALSE)
            {
                $data['error'] = '';
                
                $this->load->view('templates/header', $data);
                $this->load->view('rental/edit_car', $data);
            }
            else
            {
                if (!$this->upload->do_upload('image')) {

                    $data['title'] = "Car edited";
                    $file_name = $this->upload->data('file_name');
                    $this->rental_model->edit_car($data['car']['id']);
                    $this->load->view('templates/header', $data);
                }
                else
                {
                    if (!$this->upload->data('is_image')) {
                        $data['error'] = 'This is not image';

                        $this->load->view('templates/header', $data);
                        $this->load->view('rental/edit_car', $data);
                    }
                    else
                    {
                        $data['title'] = "Car edited";
                        $file_name = $this->upload->data('file_name');
                        $this->rental_model->edit_car($data['car']['id'],$file_name);
                        $this->load->view('templates/header', $data);
                    }
                }
            }
        }
        $this->to_menu('admin_menu');
        $this->load->view('templates/footer');
    }
    
    public function admin_menu()
    {
        $this->login_test(1);
        $data['title'] = "Admin Menu";

        $data['menu'] = array(
            array(
                'title' => 'Show cars',
                'location' => 'show_cars'
            ),
            array(
                'title' => 'Show reservations',
                'location' => 'show_res'
            ),
            array(
                'title' => 'Add',
                'location' => 'add_car'
            ),
            array(
                'title' => 'Edit',
                'location' => 'edit_car'
            ),
            array(
                'title' => 'Del',
                'location' => 'del_car'
            )
        );

        $this->load->view("templates/header", $data);
        $this->load->view("rental/menu", $data);
        $this->to_menu('');
        $this->load->view("templates/footer");
    }
    
    public function user_menu()
    {
        $this->login_test(0);
        $data['title'] = "User menu";
        $data['location'] = "user_menu";
        
        $data['menu'] = array(
            array(
                'title' => 'Show car',
                'location' => 'show_car'
            ),
            array(
                'title' => 'Show reservations',
                'show_res' => 'show_res'
            )
        );
    }
    
    private function to_menu($slug)
    {
        $to_menu = array(
            'title' => 'Menu',
            'location' => $slug
        );
        $this->load->view('rental/to_menu', $to_menu);
    }
    
    private function table($data)
    {
        if (!isset($data['options'])) {
            $data['options'] = null;
        }
        
        $data['cars'] = $this->rental_model->get_cars();
        
        $this->load->view('rental/table', $data);
    }
    
    private function form_options()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        
        $config['file_name']            = 'Car';
        $config['upload_path']          = './images/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 1000;
        $config['overwrite']            = false;
        
        $this->load->library('upload', $config);
        
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required|numeric');
    }
    private function login_test($permission)
    {
        if (!$this->session->has_userdata('username') or $this->session->userdata('permission') != $permission) {
            redirect('rental');
        }
    }
}