<table>
    <?php foreach ($cars as $row_car): ?>
    <tr>
    <td>
        <?php echo $row_car['name']; ?>
    </td>
    <td>
        <?php echo $row_car['price']; ?>
    </td>
    <td>
        <img src="<?php echo base_url('/images/'.$row_car['image']); ?>" height="100px"/>
    </td>
    <?php if($options != null) {
        echo "<td>";
        foreach ($options as $row_option)
        {
            echo sprintf('<a href="%s">%s</a><br/>',
                    site_url("rental/".$row_option['location']."/".$row_car['id']),
                    $row_option['title']);
        }
        echo "</td>";
    } ?>
    </tr>
    <?php endforeach; ?>
</table>