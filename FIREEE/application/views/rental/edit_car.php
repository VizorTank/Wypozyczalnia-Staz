<?php echo validation_errors();
    echo $error;

    echo form_open_multipart(current_url()); ?>

<label for="name">Name</label>
<input type="input" name="name" value="<?php echo $car['name']; ?>" /><br/>

<label for="price">Price</label>
<input type="number" name="price" value="<?php echo $car['price']; ?>" /><br/>

<label for="image">Image</label>
<img src="<?php echo base_url('/images/'.$car['image']); ?>" height="100px">
<input type="file" name="image" accept="image/jpeg,image/png" size=""/><br/>

<input type="submit" name="submit" value="Update car" />

</form>