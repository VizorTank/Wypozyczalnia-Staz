<?php echo validation_errors(); 
    echo $error;
    
    echo form_open_multipart('rental/add_car'); ?>

<label for="name">Name</label>
<input type="input" name="name" value="<?php echo set_value('name'); ?>" /><br/>

<label for="price">Price</label>
<input type="number" name="price" value="<?php echo set_value('price'); ?>" /><br/>

<label for="image">Image</label>
<input type="file" name="image" accept="image/jpeg,image/png" value="<?php echo set_value('image'); ?>" size=""/><br/>

<input type="submit" name="submit" value="Create car" />

</form>