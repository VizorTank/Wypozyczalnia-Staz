<?php
class Rental_model extends CI_Model {
    
    public function __construct() {
        $this->load->database();
    }
    
    public function get_cars($slug = null)
    {
        if ($slug === null)
        {
            $query = $this->db->get('cars');
            return $query->result_array();
        }
        $query = $this->db->get_where('cars', array('id' => $slug));
        return $query->row_array();
    }
    
    public function del_car($id)
    {
        $this->load->helper('file');
        $query = $this->db->get_where('cars', array('id' => $id));
        $car_image = $query->row_array();
        unlink('./images/'.$car_image['image']);
        $query = $this->db->delete('cars', array('id' => $id));
        return $query;
    }
    
    public function add_car($car_image)
    {
        $data = array(
            'name' => $this->input->post('name'),
            'price' => $this->input->post('price'),
            'image' => $car_image
        );
        
        return $this->db->insert('cars', $data);
    }
    
    public function edit_car($id, $new_image = null)
    {

        if ($new_image === null) {
            $data = array(
                'name' => $this->input->post('name'),
                'price' => $this->input->post('price')
            );
        }
        else {
            $this->load->helper('file');
            $query = $this->db->get_where('cars', array('id' => $id));
            $car_image = $query->row_array();
            unlink('./images/'.$car_image['image']);
            
            $data = array(
                'name' => $this->input->post('name'),
                'price' => $this->input->post('price'),
                'image' => $new_image
            );
        }

        $this->db->update('cars', $data, "id = ".$id);
        
    }
    
    public function show_res($slug)
    {
        $query = $this->db->get_where('reserv', array('car_id' => $slug));
        return $query->result_array();
    }
    
    public function find_user($username)
    {
        $query = $this->db->get_where('users', array('username' => $username));
        return $query->row_array();
    }
}